package com.mycompany.app;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.Test;

public class HomeworkTest {

        @Test
        public void testStats() {
                WebDriver driver = new FirefoxDriver();

                // --- Check that the page title is as expected
                driver.get("https://www.internetlivestats.com/");
                System.out.println("Page Title is " + driver.getTitle());
                Assert.assertEquals("Internet Live Stats - Internet Usage & Social Media Statistics",
                                driver.getTitle());

                // --- Check that clicking the Internet users button it takes to the view page
                WebElement findButton = driver
                                .findElement(By.className("button-population"));
                findButton.click();
                Assert.assertEquals("https://www.internetlivestats.com/watch/internet-users/", driver.getCurrentUrl());

                // --- Check that clicking on the Internet users counter it takes to the
                // information page
                WebElement usersButton = driver
                                .findElement(By.id("innercounterview-desc"));
                usersButton.click();
                Assert.assertEquals("https://www.internetlivestats.com/internet-users/", driver.getCurrentUrl());

                // --- Check that clicking on back to top button works ---
                Long defaultScroll = (Long) ((JavascriptExecutor) driver).executeScript(" return window.scrollY");

                // Scroll till the end of the page
                ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");

                // Get scroll height after scrolling till end
                Long currentScroll = (Long) ((JavascriptExecutor) driver).executeScript(" return window.scrollY");

                // Click on the back to top button
                WebElement backToTop = driver
                                .findElement(By.className("backtop"));
                backToTop.click();

                try {
                        Thread.sleep(5000);
                } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }

                // Get scroll height after clicking back to top
                Long currentScrollHeightAfterClickingOnBackToTop = (Long) ((JavascriptExecutor) driver)
                                .executeScript(" return window.scrollY");

                Assert.assertEquals(defaultScroll, currentScrollHeightAfterClickingOnBackToTop);

                // --- Search for the trend on the information page
                WebElement trendText = driver
                                .findElement(By.id("trend"));
                trendText.getText();

                driver.quit();
        }
}
